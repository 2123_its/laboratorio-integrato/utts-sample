from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, JsonResponse

from django.views import generic
from django.views import View

from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.utils.datastructures import MultiValueDictKeyError

from .models import TextAudio
# Create your views here.


# class IndexView(generic.ListView):
#     template_name = 'polls/index.html'
# def index(request):
#     tas = TextAudio.objects.all()
#     list = []
#     for ta in tas:
#         list.append({"id": ta.id, "text": ta.text})
#     return HttpResponse(f"{list}")
#
# def detail(request, textaudio_id):
#     ta = get_object_or_404(TextAudio, pk=textaudio_id)
#     return render(request, 'detail.html', {'ta': ta})

# class DetailView(generic.DetailView):
#     model = TextAudio
#     template_name = 'detail.html'

@method_decorator(csrf_exempt, name='dispatch')
class CreateGetView(View):
    def post(self, request):
        response = {"error":{
                        "code": 404,
                        "message": "audiofile not found!"
                        }
                    }
        status = 404

        try:
            text = request.POST['text']
        except MultiValueDictKeyError:
            text = None
        if text:

            ta = TextAudio() # migliorare con get_or_create
            ta.text = text
            ta.save()
            response = {"input":{
                            "text": text,
                            },
                        "output":{
                            "audiofile_url": ta.file.url
                            }

                        }
            status = 200


        return JsonResponse(response, status=status)
