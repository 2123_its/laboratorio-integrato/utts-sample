from django.db import models

# Create your models here.
class TextAudio(models.Model):
    text = models.CharField(max_length=200)
    file = models.FileField(null=True, blank=True)
    updated = models.DateTimeField(null=True, auto_now=True)
    created = models.DateTimeField(null=True, auto_now_add=True)

    def save(self, *args, **kwargs):
        # inserire qui il codice per ottenere il file da google
        # path è la variabile contenente il path del file
        # fate in modo che venga salvato nella directory MEDIA_ROOT
        # self.file.path=path
        super(TextAudio, self).save( *args, **kwargs)
