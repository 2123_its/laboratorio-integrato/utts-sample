from django.contrib import admin

from .models import TextAudio

@admin.register(TextAudio)
class TextAudioAdmin(admin.ModelAdmin):
    list_display = ('created','text',)
    search_fields = ['text',]
    list_filter = ['created',]
    readonly_fields = ['file']
